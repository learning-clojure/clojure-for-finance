;; Primitives
user=> 1
1
user=> "foobar"
"foobar"
user=> (+ 1 1)
2
user=> (+ 5 (* 2 4 6))
53
user=> nil
nil
user=> (type nil)
nil
user=> true
true
user=> (type false)
java.lang.Boolean
user=> (+ 9 5)
14
user=> (- 400 18.75)
381.25
user=> (* 6 10)
60
user=> (/ 50 5)
10
user=> (quot 50 5)
10
user=> (rem 26 12)
2
user=> (mod 26 12)
2
user=> (inc 67)
68
user=> (dec 100)
99
user=> (max 250 45)
250
user=> (min 250 45)
45

user=> (reverse "hello")
(\o \l \l \e \h)
user=> (count "hello")
5
user=> (first "hello")
\h

user=> (require '[clojure.string :as s])
nil
user=> (s/reverse "hello")
"olleh"
user=> (reverse "hello")
(\o \l \l \e \h)
user=> (s/join ", " "hello")
"h, e, l, l, o"

user=> (re-find #"\d+" "hello1234")
"1234"
user=> (s/replace "Hello World" #"World" "Clojure")
"Hello Clojure"

;; Symbols
user=> (re-find #"\d+" "hello1234")
"1234"
user=> (s/replace "Hello World" #"World" "Clojure")
"Hello Clojure"
user=> (symbol 'hello)
hello
user=> (symbol "hello")
hello
user=> (symbol "clojure.core" "hello")
clojure.core/hello
user=> (def hello "world")
#'user/hello
user=> hello
"world"

;; Keywords
user=> :hello
:hello
user=> (:foo {:foo "bar" :hello "world"})
"bar"

;; Collections
;; Lists
user=> (conj '("a" "s" "d" "f") "z")
("z" "a" "s" "d" "f")
user=> (count '("a" "s" "d" "f"))
4
user=> (empty '("a" "s" "d" "f"))
()
user=> (empty? '("a" "s" "d" "f"))
false

;; Vectors
user=> [75 6 452 40]
[75 6 452 40]
user=> (conj ["a" "s" "d" "f"] "z")
["a" "s" "d" "f" "z"]
user=> (mapv inc [1 2 3 4])
[2 3 4 5]

;; Sets
user=> #{1 2 3 4}
#{1 4 3 2}
user=> (#{"a" "s" "d" "f"} 2)
nil
user=> (#{"a" "s" "d" "f"} "d")
"d"

;; Maps
user=> {:hello "world" :foo "bar"}
{:hello "world", :foo "bar"}
user=> ({:hello "world" :foo "bar"} :foo)
"bar"
user=> (assoc {} :a "b")  ;; {:a "b"}
{:a "b"}
user=> (zipmap [:hello :foo] ["world" "bar"])
{:hello "world", :foo "bar"}
