(ns clj-finance.analytics
  (:require [clj-finance.core :as core]))

(defn simple-moving-average
  "Options are:
     :input - input key function will look for (defaults to :last-trade-price)
     :output - output key function will emit (defaults to :last-trade-price-average)
     :etal - other keys to emit in each result map
     ** This function assumes the latest tick is on the left**"
  [options tick-window tick-list]
  (let [start-index tick-window

        {input-key :input
         output-key :output
         etal-key :etal
         :or {input-key :last-trade-price
              output-key :last-trade-price-average
              etal-key [:last-trade-price :last-trade-time]}} options]

    (reduce (fn [rslt ech]

              (let [tsum (reduce (fn [rr ee]
                                   (let [ltprice (:last (:last-trade-price ee))]
                                     (+ ltprice rr))) 0 ech)
                    taverage (/ tsum (count ech))]

                (lazy-cat rslt
                          [(merge
                             (zipmap etal-key
                                     (map #(% (last ech)) etal-key))
                             {output-key taverage
                              :population ech})])))
            '()
            (partition tick-window
                       1
                       (take (* 2 tick-window) tick-list)))))
